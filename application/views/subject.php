
        <div class="wrapper">
            <div class="container">
                <h1 class="page-title">$subject_name</h1>
                <div class="row">
                    <!-- CHART -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-8">
                        <div class="subject-info col-lg-3 col-md-3 col-sm-3 col-xs-1">
                            <h2>$subject_name</h2>
                            <h1>$subject_id</h1>
                            <h4>$subject_description</h4>
                        </div>
                        <div class="subject-add-info col-lg-6 col-md-6 col-sm-6 col-xs-4">
                            <p>$subject_units</p>
                            <p>$subject_equivalencies</p>
                            <p>$subject_prerequisites</p>
                            <p>$subject_bloque</p>
                        </div>
                    </div>
                    <!-- TABLE -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-8">
                        <div class="table">
                            <table id="" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Matrícula</th>
                                        <th>Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th>Apellido materno</th>
                                        <th>Prioridad</th>
                                        <th>Correo</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    <tr>
                                        <td>$student_id</td>
                                        <td>$student_first_name</td>
                                        <td>$student_middle_name</td>
                                        <td>$student_last_name</td>
                                        <td>$student_subject_priority</td>
                                        <td>$student_alt_email</td>
                                    </tr>
                                    <tr>
                                        <td>$student_id</td>
                                        <td>$student_first_name</td>
                                        <td>$student_middle_name</td>
                                        <td>$student_last_name</td>
                                        <td>$student_subject_priority</td>
                                        <td>$student_alt_email</td>
                                    </tr>
                                    <tr>
                                        <td>$student_id</td>
                                        <td>$student_first_name</td>
                                        <td>$student_middle_name</td>
                                        <td>$student_last_name</td>
                                        <td>$student_subject_priority</td>
                                        <td>$student_alt_email</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> <!-- END TABLE -->
                    <form name="genera reporte">
                        <input type="button" name="btn Reporte" value ="Genera reporte"></input>
                    </form>
                </div>
            </div>
        </div>

        <!--<span class="add-icon glyphicon glyphicon-plus" aria-hidden="true"></span>-->

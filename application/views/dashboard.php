        <div class="wrapper">
            <div class="container">
                <h1 class="page-title">Materias por prioridad</h1>
                
                <div class="row">
                    
                    <!-- CHART -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-11">
                        <div class="chart">
                            <div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
                        </div>
                        <div class="chart-info">

                        </div><!-- END CHART -->
                    </div>
                    <!-- TABLE -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-8">
                        <div class="table">
                            <table id="" class="display" cellspacing="0" width="100%">
                                    <hr>
                                    <h2>$subject_id</h2>
                                    <thead>
                                        <tr>
                                            <th>Matrícula</th>
                                            <th>Nombre</th>
                                            <th>Apellido paterno</th>
                                            <th>Apellido materno</th>
                                            <th>Prioridad</th>
                                            <th>Correo</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td>$student_id</td>
                                            <td>$student_first_name</td>
                                            <td>$student_middle_name</td>
                                            <td>$student_last_name</td>
                                            <td>$student_subject_priority</td>
                                            <td>$student_alt_email</td>
                                        </tr>
                                        <tr>
                                            <td>$student_id</td>
                                            <td>$student_first_name</td>
                                            <td>$student_middle_name</td>
                                            <td>$student_last_name</td>
                                            <td>$student_subject_priority</td>
                                            <td>$student_alt_email</td>
                                        </tr>
                                        <tr>
                                            <td>$student_id</td>
                                            <td>$student_first_name</td>
                                            <td>$student_middle_name</td>
                                            <td>$student_last_name</td>
                                            <td>$student_subject_priority</td>
                                            <td>$student_alt_email</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                        <div class="table">
                            <table id="" class="display" cellspacing="0" width="100%">
                                <hr>
                                <h2>$subject_id</h2>
                                <thead>
                                    <tr>
                                        <th>Matrícula</th>
                                        <th>Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th>Apellido materno</th>
                                        <th>Prioridad</th>
                                        <th>Correo</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>$student_id</td>
                                        <td>$student_first_name</td>
                                        <td>$student_middle_name</td>
                                        <td>$student_last_name</td>
                                        <td>$student_subject_priority</td>
                                        <td>$student_alt_email</td>
                                    </tr>
                                    <tr>
                                        <td>$student_id</td>
                                        <td>$student_first_name</td>
                                        <td>$student_middle_name</td>
                                        <td>$student_last_name</td>
                                        <td>$student_subject_priority</td>
                                        <td>$student_alt_email</td>
                                    </tr>
                                    <tr>
                                        <td>$student_id</td>
                                        <td>$student_first_name</td>
                                        <td>$student_middle_name</td>
                                        <td>$student_last_name</td>
                                        <td>$student_subject_priority</td>
                                        <td>$student_alt_email</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                            
                        <div class="table">
                            <table id="" class="display" cellspacing="0" width="100%">
                                <hr>
                                <h2>$subject_id</h2>
                                <thead>
                                    <tr>
                                        <th>Matrícula</th>
                                        <th>Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th>Apellido materno</th>
                                        <th>Prioridad</th>
                                        <th>Correo</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>$student_id</td>
                                        <td>$student_first_name</td>
                                        <td>$student_middle_name</td>
                                        <td>$student_last_name</td>
                                        <td>$student_subject_priority</td>
                                        <td>$student_alt_email</td>
                                    </tr>
                                    <tr>
                                        <td>$student_id</td>
                                        <td>$student_first_name</td>
                                        <td>$student_middle_name</td>
                                        <td>$student_last_name</td>
                                        <td>$student_subject_priority</td>
                                        <td>$student_alt_email</td>
                                    </tr>
                                    <tr>
                                        <td>$student_id</td>
                                        <td>$student_first_name</td>
                                        <td>$student_middle_name</td>
                                        <td>$student_last_name</td>
                                        <td>$student_subject_priority</td>
                                        <td>$student_alt_email</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                            
                            
                        <div class="table">
                            <table id="" class="display" cellspacing="0" width="100%">
                                <hr>
                                <h2>$subject_id</h2>
                                <thead>
                                    <tr>
                                        <th>Matrícula</th>
                                        <th>Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th>Apellido materno</th>
                                        <th>Prioridad</th>
                                        <th>Correo</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>$student_id</td>
                                        <td>$student_first_name</td>
                                        <td>$student_middle_name</td>
                                        <td>$student_last_name</td>
                                        <td>$student_subject_priority</td>
                                        <td>$student_alt_email</td>
                                    </tr>
                                    <tr>
                                        <td>$student_id</td>
                                        <td>$student_first_name</td>
                                        <td>$student_middle_name</td>
                                        <td>$student_last_name</td>
                                        <td>$student_subject_priority</td>
                                        <td>$student_alt_email</td>
                                    </tr>
                                    <tr>
                                        <td>$student_id</td>
                                        <td>$student_first_name</td>
                                        <td>$student_middle_name</td>
                                        <td>$student_last_name</td>
                                        <td>$student_subject_priority</td>
                                        <td>$student_alt_email</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                            
                        <div class="table">
                            <table id="" class="display" cellspacing="0" width="100%">
                                    <hr>
                                    <h2>$subject_id</h2>
                                    <thead>
                                        <tr>
                                            <th>Matrícula</th>
                                            <th>Nombre</th>
                                            <th>Apellido paterno</th>
                                            <th>Apellido materno</th>
                                            <th>Prioridad</th>
                                            <th>Correo</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td>$student_id</td>
                                            <td>$student_first_name</td>
                                            <td>$student_middle_name</td>
                                            <td>$student_last_name</td>
                                            <td>$student_subject_priority</td>
                                            <td>$student_alt_email</td>
                                        </tr>
                                        <tr>
                                            <td>$student_id</td>
                                            <td>$student_first_name</td>
                                            <td>$student_middle_name</td>
                                            <td>$student_last_name</td>
                                            <td>$student_subject_priority</td>
                                            <td>$student_alt_email</td>
                                        </tr>
                                        <tr>
                                            <td>$student_id</td>
                                            <td>$student_first_name</td>
                                            <td>$student_middle_name</td>
                                            <td>$student_last_name</td>
                                            <td>$student_subject_priority</td>
                                            <td>$student_alt_email</td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                    </div> <!-- END TABLE -->
                </div> <!-- END ROW -->
            </div><!--END CONTAINTER-->
        </div> <!--END WRAPPER-->

        <!--<span class="add-icon glyphicon glyphicon-plus" aria-hidden="true"></span>-->
        
    
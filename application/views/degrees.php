        <div class="wrapper">
            <div class="container">
                <h1 class="page-title">Carreras</h1>
                <div class="row">
                    <!-- CHART -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-8">
                        <div class="degree-info col-lg-3 col-md-3 col-sm-3 col-xs-1">
                            <h1>Carreras</h1> 
                        </div>
                        <div class="degree-add-info col-lg-6 col-md-6 col-sm-6 col-xs-4">
                        </div>
                    </div>
                    <!-- TABLE -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-8">
                    <ul class="nav nav-tabs">
                    </ul>
                        <div class="table">
                            <table id="" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Significado</th>
                                        <th>Plan</th>
                                        <th>Director</th>
                                        <th>Numero de alumnos</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    <tr>
                                        <td>$degree_id</td>
                                        <td>$degree_name</td>
                                        <td>$plan_number</td>
                                        <td>$director_degree_name</td>
                                        <td>$students_number</td>
                                    </tr>
                                    <tr>
                                        <td>$degree_id</td>
                                        <td>$degree_name</td>
                                        <td>$plan_number</td>
                                        <td>$director_degree_name</td>
                                        <td>$students_number</td>
                                    </tr>
                                    <tr>
                                        <td>$degree_id</td>
                                        <td>$degree_name</td>
                                        <td>$plan_number</td>
                                        <td>$director_degree_name</td>
                                        <td>$students_number</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> <!-- END TABLE -->
                </div>
            </div>
        </div>

        <!--<span class="add-icon glyphicon glyphicon-plus" aria-hidden="true"></span>-->
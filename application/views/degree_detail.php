        <div class="wrapper">
            <div class="container">
                <h1 class="page-title">Materias por prioridad</h1>
                <div class="row">
                    <!-- CHART -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-8">
                        <div class="degree-info col-lg-3 col-md-3 col-sm-3 col-xs-1">
                            <h1>$degree_plan</h1>
                            <h4>$degree_description</h4>
                        </div>
                        <div class="degree-add-info col-lg-6 col-md-6 col-sm-6 col-xs-4">
                            <p>$degree_department</p>
                            <p>$degree_director</p>
                        </div>
                    </div>
                    <!-- TABLE -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-8">
                    <ul class="nav nav-tabs">
                        <li role="presentation" id="students">Alumnos</li> <!-- class="active"-->
                        <li role="presentation" id="subjects">Materias</li>
                    </ul>
                        <div class="table">
                            <table id="" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Matrícula</th>
                                        <th>Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th>Apellido materno</th>
                                        <th>Prioridad</th>
                                        <th>Correo</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    <tr>
                                        <td>$student_id</td>
                                        <td>$student_first_name</td>
                                        <td>$student_middle_name</td>
                                        <td>$student_last_name</td>
                                        <td>$student_subject_priority</td>
                                        <td>$student_alt_email</td>
                                    </tr>
                                    <tr>
                                        <td>$student_id</td>
                                        <td>$student_first_name</td>
                                        <td>$student_middle_name</td>
                                        <td>$student_last_name</td>
                                        <td>$student_subject_priority</td>
                                        <td>$student_alt_email</td>
                                    </tr>
                                    <tr>
                                        <td>$student_id</td>
                                        <td>$student_first_name</td>
                                        <td>$student_middle_name</td>
                                        <td>$student_last_name</td>
                                        <td>$student_subject_priority</td>
                                        <td>$student_alt_email</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> <!-- END TABLE -->
                </div>
            </div>
        </div>

        <!--<span class="add-icon glyphicon glyphicon-plus" aria-hidden="true"></span>-->
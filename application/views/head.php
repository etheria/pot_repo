<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- STATIC -->
        <title>Planeación de materias</title>

        <meta charset="utf-8">

        <!-- CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"><!-- bootstrap -->
        <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.5/css/jquery.dataTables.css"><!-- datatables -->
        
        
        <link rel="stylesheet" type="text/css" href="../../static/DataTables/media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="../../static/DataTables/examples/resources/syntax/shCore.css">
        <link rel="stylesheet" type="text/css" href="../../static/DataTables/examples/resources/demo.css">
        <style type="text/css" class="init">

        div.dataTables_wrapper {
            margin-bottom: 3em;
        }
        </style>
        
        <!-- JS -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script><!-- jquery -->
        <script type="text/javascript" charset="utf8" src="http://code.jquery.com/jquery-1.10.2.min.js"></script><!-- jquery -->
        <script src="../js/charts/highcharts.js"></script><!-- highcharts -->
        <script src="../../static/js/charts/modules/exporting.js"></script><!-- export highcharts -->
        <script src="../../static/js/charts/test_report.js"></script><!-- test highcharts -->
        <script src="../../static/js/tables/datatable.js"></script><!-- test datatables-->
        <script type="text/javascript" charset="utf8" src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.js"></script><!-- datatables-->
        <script type="text/javascript" language="javascript" class="init">
            $(document).ready(function() {
                $('table.display').dataTable();
            } );
        </script>
        <script type="text/javascript" language="javascript" src="../../static/Datatables/examples/resources/syntax/shCore.js"></script>
        <script type="text/javascript" language="javascript" src="../../static/DataTables/examples/resources/demo.js"></script>
        <!--<script type="text/javascript" language="javascript" src="Datatables/media/js/jquery.js"></script>
        
        
        -->

    </head>
    
    <body>
        
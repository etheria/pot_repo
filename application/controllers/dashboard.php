<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Dashboard extends CI_Controller {
    
    public function view($dashboard = 'home')
    {
    	if(! file_exists(APPPATH.'/views/'.$dashboard.'.php'))
    	{
    		show_404();
    	}

    
    }

    public function index() 
    {
    	$this->load->view('header');
        $this->load->view('dashboard');
        $this->load->view('footer');
    }

    public function show_404()
    {
    	$this->load->view('404');

    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class degree extends CI_Controller {
     
    public function index() 
    {
    	$this->load->view('header');
        $this->load->view('degrees');
        $this->load->view('footer');
    }
}